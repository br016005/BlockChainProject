# BlockChainProject
CS3BC19 BlockChain Coursework


NOTE: 
  To Turn on / off Multi Threading, change THREADING property (Defined Top of block.cs)
  
  To Turn on / off Transaction Validation (Key Check + Sufficient Fund Check) change "testtrans" in blockchainApp.cs
  
This program should be fully functional. If any errors occur please send an email @ BR016005@student.reading.ac.uk 
